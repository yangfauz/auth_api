<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function(){
    Route::post('/register', 'Api\v1\AuthController@register');
    Route::post('/login', 'Api\v1\AuthController@login');
    
    Route::middleware('auth:sanctum')->group(function(){
        Route::get('/list-user/{data_show}', 'Api\v1\UserController@listUser');

        Route::post('/logout', 'Api\v1\AuthController@logout');
    });
});
