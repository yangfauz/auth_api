<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function listUser($data_show)
    {
        try{
            $users = User::select('id', 'name', 'email')->orderBy('id', 'DESC')->get();
            
            return response()->json([
                'meta' => [
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'get data users success'
                ],
                'data' => $users
            ], 200);
        } catch (Exception $error) {
            return response()->json(["code" => 401, "message" => $error], 401);
        }
    }
}
