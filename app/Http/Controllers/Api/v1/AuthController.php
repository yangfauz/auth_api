<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'name' => 'string|required',
                'email' => 'string|required|email|unique:users,email',
                'password' => 'string|required|min:6',
            ]);

            if($validator->fails()){
                return response()->json(['error' => $validator->errors()], 400);
            }

            $request->password = bcrypt($request->password);

            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->save();

            if($user){
                $token = $user->createToken('auth_api')->plainTextToken;

                return response()->json([
                    'meta' => [
                        'code'      => 200,
                        'status'    => 'success',
                        'message'   => 'register user success'
                    ],
                    'data' => [
                        'name'  => $user->name,
                        'email' => $user->email,
                        'token' => $token
                    ]
                ], 200);
            }else{
                return response()->json([
                    'meta' => [
                        'code'      => 401,
                        'status'    => 'error',
                        'message'   => 'register user failed'
                    ],'data' => null
                ], 401);
            }
        } catch (Exception $error) {
            return response()->json(["code" => 401, "message" => $error], 401);
        }
    }

    public function login(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'email' => 'string|required',
                'password' => 'string|required|min:6'
            ]);

            if($validator->fails()){
                return response()->json(['error' => $validator->errors()], 400);
            }

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $user = Auth::user();
 
                $token = $user->createToken('auth_api')->plainTextToken;

                return response()->json([
                    'meta' => [
                        'code'      => 200,
                        'status'    => 'success',
                        'message'   => 'login success'
                    ],
                    'data' => [
                        'token' => $token
                    ]
                ], 200);
            }else{
                return response()->json([
                    'meta' => [
                        'code'      => 401,
                        'status'    => 'error',
                        'message'   => 'invalid credential'
                    ],'data' => null
                ], 401);
            }
        } catch (Exception $error) {
            return response()->json(["code" => 401, "message" => $error], 401);
        }
    }

    public function logout()
    {
        try{
            if (Auth::user()) {
                Auth::user()->tokens()->delete();
            }
            return response()->json([
                'meta' => [
                    'code'      => 200,
                    'status'    => 'success',
                    'message'   => 'logout success'
                ],
                'data' => null
            ], 200);
        } catch (Exception $error) {
            return response()->json(["code" => 401, "message" => $error], 401);
        }
    }
}
